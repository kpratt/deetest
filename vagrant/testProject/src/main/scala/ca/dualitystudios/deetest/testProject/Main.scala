package ca.dualitystudios.deetest.testProject

import ca.dualitystudios.deetest.{DeeTest, Run, SYSTEM_NAME, Controller}
import akka.pattern.ask
import akka.actor.ActorSystem
import scala.collection.JavaConverters._
import com.typesafe.config.ConfigFactory
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit

object Main extends App {

  val config = ConfigFactory.load();

  implicit val sys = ActorSystem(SYSTEM_NAME, config)

  val controller = Controller()
  val future = controller.ask(Run(new TheTest))(DeeTest.timeout)
  val results = Await.result(future, Duration(10, TimeUnit.MINUTES))
  println("=========")
  println(results)
  println("=========")
  sys.shutdown()
  System.exit(0)
}
