package ca.dualitystudios.deetest


import scala.concurrent.{Await, Future}
import akka.util.Timeout
import java.util.concurrent.TimeUnit._
import akka.actor.{ActorSelection, ActorRef}
import akka.pattern.ask
import Implicits._
import scala.reflect.ClassTag

object Implicits {
  implicit val defaultTimeout = Timeout(3, MINUTES)
  implicit def toAwaitable[T](future: Future[T]): Awaitable[T] = new Awaitable(future)
  implicit def toQuestion(ref: ActorRef): RefQuestion = new RefQuestion(ref)
  implicit def toQuestion(ref: ActorSelection): SelectQuestion = new SelectQuestion(ref)
}

class Awaitable[T](future: Future[T]) {
  def await: T = Await.result(future, Implicits.defaultTimeout.duration)
}

class RefQuestion(ref: ActorRef) {
  def askNow[T:ClassTag](q: Any) = ref.ask(q).mapTo[T].await
}
class SelectQuestion(ref: ActorSelection) {
  def askNow[T:ClassTag](q: Any) = ref.ask(q).mapTo[T].await
}
