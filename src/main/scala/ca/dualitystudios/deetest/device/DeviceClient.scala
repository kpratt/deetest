package ca.dualitystudios.deetest.device

import java.io.File
import org.apache.commons.io.FileUtils
import akka.actor.{ActorRef, TypedActor, ActorSystem}
import ca.dualitystudios.dsg.Tracker
import com.typesafe.config.{Config, ConfigFactory}
import java.util.UUID
import scala.util.Try
import ca.dualitystudios.deetest._

object DeviceClient extends App with Logger {


  val dirIndex: Int = args.indexOf("--app-dir")
  println("dirIndex = [ "+dirIndex+" ]")
  println("args = [ "+args.mkString(",")+" ]")
  val appDir =
    if(dirIndex >= 0){
      new File(args(dirIndex + 1))
    } else new File(".")
  appDir.mkdirs()

  val debug = args.contains("--debug")

  val configFile = new File(appDir.getAbsolutePath + "/application.conf")
  if(! configFile.exists()) {
    configFile.createNewFile()
    FileUtils.write(configFile,
      "deetest.slave.uuid=\""+UUID.randomUUID().toString+"\"\n"+
        "deetest.tracker.host=localhost\n"+
        "deetest.tracker.port=9000\n"+
        "akka.remote.netty.tcp.port=2552\n"
    )

  }

  val config = ConfigFactory.parseFile(configFile).withFallback(ConfigFactory.load())

  println("=====================")
  println("akka.actor.provider [ "+config.getString("akka.actor.provider")+" ]")
  Try { new DeviceClient(args.contains("--no-track"), config).run }.recover {
    case e => e.printStackTrace();System.exit(1)
  }

  //wait forever
}

class DeviceClient(noTrack: Boolean, conf: Config) {

  def run: ActorRef = {

    val track = !noTrack
    val config = conf.withFallback(ConfigFactory.load())
    val sysName = (if(config.hasPath("deetest.system.name")) config.getString("deetest.system.name") else SYSTEM_NAME)
    implicit val sys = ActorSystem(sysName, config)
    val slave = Slave()
    println("DEBUG: Slave path [" + slave.path +"]")
    slave ! Print("Slave is receiving.")

    if(track) {
      println("DEBUG: Tracking at " + config.getString("deetest.tracker.host"))
      implicit val tracker = new Tracker(config.getString("deetest.tracker.host"), config.getInt("deetest.tracker.port"), DeviceClient.debug)
      // try every 10 seconds until success
      while(! Try{
    		  tracker.register( config.getString("deetest.slave.uuid"), None,  config.getInt("akka.remote.netty.tcp.port"))
        }.getOrElse(false)
      ) {
        Thread.sleep(10*1000)
      }
      println("DEBUG: Registered at tracker.")
    }

    slave
  }
}
