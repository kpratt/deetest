package ca.dualitystudios.deetest.testProject


import ca.dualitystudios.deetest.{TestStructure, Spec}

import java.util.concurrent.atomic.{AtomicInteger, AtomicReference}


class TheTest extends Spec with Serializable {

  val actors: List[String] = List(
    "slave"
  )


  def test: TestStructure = List(
    device("slave") { case context: AtomicInteger =>
      println(context.get())
    }
  )


  def setup: PartialFunction[String, Any] = {

    case "slave" => new AtomicInteger(1)

  }

}
