package ca.dualitystudios.deetest

import org.slf4j.LoggerFactory

trait Logger {
  val logger = LoggerFactory.getLogger(this.getClass)

  def printf(formatString: String, args: AnyRef*) = logger.debug(formatString, args)
  def println(msg: String) = logger.debug(msg)
  def print(msg: String) = logger.debug(msg)
}
