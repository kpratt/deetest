package ca.dualitystudios.deetest

abstract class TestResult {
  def &(other: TestResult): TestResult
}

case object Success extends TestResult {
  def &(other: TestResult) = other
}

case class Failure(val message: String) extends TestResult {
  def &(other: TestResult) = other match {
    case o: Failure => MultipleFailures(List(this, o))
    case m: MultipleFailures => MultipleFailures(this :: m.fail)
    case _ => this
  }
}

case class Error(message: String, trace: List[String]) {
  implicit def toFailure: TestResult = Failure(trace.mkString(message+"\n", "\n\t", ""))
}

case class MultipleFailures(fail: List[Failure]) extends TestResult {
  def &(other: TestResult) = other match {
    case o: Failure => o & this
    case m: MultipleFailures => MultipleFailures(this.fail ++ m.fail)
    case _ => this
  }
}

//=================================

trait Asserts {
  implicit def fromUnit(x:Unit) = Success

  def assert(failureMessage: String)(bool: => Boolean) =
    if(bool) Success else Failure(failureMessage)
  def success = Success
  def failure(msg: String) = Failure(msg)
}

//=================================

object SpecException {
  def apply(msg: String) = new RuntimeException(msg) with SpecException
  def apply(msg: String, cause: Throwable) = new RuntimeException(msg,cause) with SpecException
}
trait SpecException {
  self: RuntimeException =>
}
