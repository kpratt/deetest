package ca.dualitystudios.deetest


import akka.pattern.ask
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import java.util.concurrent.{TimeoutException, TimeUnit}
import akka.actor.ActorRef
import Implicits._
import akka.util.Timeout
import TimeUnit._
import scala.util.Try
import java.util.Date

object DeeTest {

  val timeout = Timeout(30, MINUTES)

  trait AsSpec2Result {

    self: Spec =>

    def asResult(controller: ActorRef): org.specs2.execute.Result = {
      val result = Try {
        val futureTestResults = controller.ask(Run(this))(timeout)
        val testResults = Await.result(futureTestResults, timeout.duration)
        testResults match {
          case Success => org.specs2.execute.Success()
          case Failure(msg) => org.specs2.execute.Failure(msg)
          case _ => org.specs2.execute.Failure("We don't know why this failed.")
        }
      } recover {
        case e: Exception => org.specs2.execute.Failure("Exception " + e.getMessage)
      } get

      result match {
        case org.specs2.execute.Failure(msg, _, _, _) => println("==\nFailure: " + msg + "\n==")
        case _ =>
      }

      result
    }
  }

  def waitUntil(f: () => Boolean, timeout: Duration): Try[Unit] = {
    Try {
      val start = new Date()
      while(! f()){
        if(start.getTime + timeout.toMillis < new Date().getTime){
          throw new TimeoutException()
        } else {
          Thread.sleep(100)
        }
      }
      ()
    }
  }

  def waitUntil[T](timeout: Duration, f: () => T): Try[T] = {
    Try {
      val start = new Date()
      var g = f()
      while(g == null){
        if(start.getTime + timeout.toMillis < new Date().getTime){
          throw new TimeoutException()
        } else {
          Thread.sleep(100)
          g = f()
        }
      }
      g
    }
  }

}
