package ca.dualitystudios.deetest.external.c4i

object CameraSimulator {

  def apply() = new CameraSimulator

}

class CameraSimulator {
  def simulatePairingGesture() = ()
  def putPersonLocation(person: String, x: Int, y: Int) = ()
}
