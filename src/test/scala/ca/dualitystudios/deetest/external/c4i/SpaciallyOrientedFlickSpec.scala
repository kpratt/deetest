package ca.dualitystudios.deetest.external.c4i

import ca.dualitystudios.deetest._
import ca.dualitystudios.deetest.Failure
import ca.dualitystudios.deetest.TestStructure._
import ca.dualitystudios.deetest.Failure

object SpaciallyOrientedFlickSpec extends Spec {

  override val actors = List(
    "tablet",
    "table",
    "camera"
  )

  override def setup = {
    case "tablet" => TabletClient()
    case "table" => TableClient()
    case "camera" => CameraSimulator()
  }

  override def test = List(
    device("table"){ case ctx: TableClient =>
      ctx.startC4i()
      ()
    },

    device("tablet"){ case ctx: TabletClient =>
      ctx.startC4i()
      ctx.pairWithUser()
      ()
    },

    device("camera"){ case ctx: CameraSimulator =>
      ctx.simulatePairingGesture()
      ()
    },

    device("tablet"){ case ctx: TabletClient =>
      val lat = 5
      val long = 5
      val zoom = 2
      ctx.openMap(lat, long, zoom)
      ()
    },

    device("camera"){ case ctx: CameraSimulator =>
      val person = "first"
      val x = 23
      val y = 32
      ctx.putPersonLocation(person, x, y)
      ()
    },

    device("tablet"){ case ctx: TabletClient =>
      val orientation = 90
      ctx.orientDevice(orientation)
      ctx.flickUp()
      ()
    },

    device("table"){ case ctx: TableClient =>
      // confirm tablets map view is now show on table
      val lat = 5
      val long = 5
      val zoom = 2
      if( ! ctx.mapIsAt(lat, long, zoom) ){
        Failure("The map did not open to the desired location.")
      } else {
        Success
      }
    }
  )
}