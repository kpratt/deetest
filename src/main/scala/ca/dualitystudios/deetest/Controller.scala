package ca.dualitystudios.deetest

import akka.actor._
import scala.concurrent.{Future, Await}
import ca.dualitystudios.dsg.Tracker
import scala.collection.JavaConverters._
import com.typesafe.config.Config
import Implicits._
import ca.dualitystudios.deetest.device._
import ca.dualitystudios.deetest.device.Execute
import ca.dualitystudios.deetest.device.Teardown
import akka.actor.ActorIdentity
import akka.actor.Identify
import ca.dualitystudios.deetest.device.Setup
import ca.dualitystudios.deetest.device.Allocate
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration

import scala.concurrent.ExecutionContext.Implicits.global

object Controller extends Logger {
  def apply()(implicit sys: ActorSystem): ActorRef = {
      val locators: Iterable[(String, String, Int)] =
        if(sys.settings.config.getBoolean("deetest.track")) {
          locatorsFromTracker(sys.settings.config)
        } else {
          locatorsFromConfig(sys.settings.config)
        }


      println(locators.map(_._1).mkString("Found actors [ ", ", ", " ]"))
      val actorRefs: Map[String, ActorRef] = locators.map {
        loc =>
          val (name, domain, port) = loc
          val actorSelection = sys.actorSelection(actorPath(domain, port))
          actorSelection ! Print("========== akka, do you speak it!!?")
          val actorRef = actorSelection.askNow[ActorIdentity](Identify(None)).getRef
          println("Got ActorRef for [ " + name + " ]")
          (name -> actorRef)
      }.toMap
      println("Building controller.")
      sys.actorOf(
        Props(new Controller(actorRefs))
      )
    }

  def actorPath(domain: String, port: Int) =
    ActorPath.fromString("akka.tcp://" + SYSTEM_NAME + "@" + domain + ":" + port + "/user/slave")

  def apply(factory: => Controller)(implicit sys: ActorSystem): ActorRef = {
    sys.actorOf(
      Props(factory)
    )
  }


  private def locatorsFromConfig(config: Config): Iterable[(String, String, Int)] = {
    val deetest_devices = config.getStringList("deetest.devices")
    deetest_devices.asScala.
      map(_.split('|')).
      map(x => (x(0).trim, x(2).trim, x(3).trim.toInt))
  }

  private def locatorsFromTracker(config: Config): Iterable[(String, String, Int)] = {
    val deetest_devices = config.getStringList("deetest.devices")
    val uuids = deetest_devices.asScala.
      map(_.split('|')).
      map(x => x(1).trim -> x(0).trim).
      toMap
    val tracker = new Tracker(
      config.getString("deetest.tracker.host"),
      config.getInt("deetest.tracker.port")
    )
    tracker.findUsers(uuids.keys).map( x =>
      (uuids(x._1), x._2, x._3)
    )
  }

}

abstract class ControllerEvent
case class Run(spec: Spec) extends ControllerEvent
case class Allocated(name: String) extends ControllerEvent
case class RequestAllocate(name: String) extends ControllerEvent
case object SetUp extends ControllerEvent
case object TearDown extends ControllerEvent


abstract class ControllerState
case object Idle extends ControllerState
case object Allocating extends ControllerState
case object RunningTest extends ControllerState
case object CleaningUp extends ControllerState

abstract class ControllerData
case object Nil extends ControllerData
case class Instruction(i: Int, running: Running) extends ControllerData
case class Result(i: TestResult, running: Running) extends ControllerData
case class Running(
    val spec: Spec,
    val remoteFinder: ActorRef,
    val monitor: ActorRef,
    val components: Set[String] = Set())
  extends ControllerData {

  def hasAllResources = spec.actors.size == components.size
  def withResource(slave: String) = this copy(components = (this.components + slave))
}

class Controller(actorRefs: Map[String, ActorRef])
  extends Actor
  with FSM[ControllerState, ControllerData]
  with Logger {

  val components: List[String] = actorRefs.keys.toList.sorted

  startWith(Idle, Nil)

  when(Idle) {
    case Event(Run(spec), Nil) =>

      components.headOption map { next =>
        println("Running spec [ " +spec.getClass.getName+ " ]")
        val remoteFinder = RemoteClassFinder()(context)
        val replyTo = sender
        self ! RequestAllocate(next)
        goto(Allocating) using Running(spec, remoteFinder, replyTo)
      } getOrElse {
        println("No components. Maybe an error is appropriate?")
        sender ! Failure("No components. Maybe an error is appropriate?")
        goto(Idle) using Nil
      }
  }
  // RemoteClassFinder()(context)
  when(Allocating) {
    case Event(AllocationFailed(name), running: Running) =>
      println("Could not allocate [ "+name+" ] retrying in one minute.")
      context.system.scheduler.scheduleOnce(Duration(1, TimeUnit.MINUTES)){
        self ! RequestAllocate(name)
      }
      goto(Allocating) using running

    case Event(RequestAllocate(name), running: Running) =>
      actorRefs(name) ! Allocate(name, running.remoteFinder)
      goto(Allocating) using running

    case Event(Allocated(name), running: Running) =>
      val data = running withResource name
      if(!data.hasAllResources) {
        val next = components.drop(data.components.size).head
        println("Now allocating [ "+next+" ]")
        self ! RequestAllocate(next)
        goto(Allocating) using data
      } else {
        println("Allocation complete")
        self ! SetUp
        goto(RunningTest) using data
      }
  }

  when(RunningTest) {
    case Event(SetUp, running:Running) =>
      println("Setting up the test.")
      running.components.foreach {
        name =>
          actorRefs(name) ! Setup(name, running.spec.getClass.getName)
      }
      Thread.sleep(5000)
      self ! Success
      println("going to run")
      goto(RunningTest) using Instruction(0, running)

    case Event(Success, Instruction(i, running)) =>
      println("Running instruction [ "+i+ "]")
      val testOp = running.spec.test.testInstance(0)
      if(testOp.isDefined) {
        val test = testOp.get
        test.drop(i).headOption.map {
          pair =>
            val (device, block) = pair
            println("sending func class")
            actorRefs(device) ! Execute(i)
            goto(RunningTest) using Instruction(i+1, running)
        } getOrElse {
          println("No more instructions going to Clean up")
          self ! TearDown
          goto(CleaningUp) using Result(Success, running)
        }
      } else {
        println("No test def!!!!")
        self ! TearDown
        goto(CleaningUp) using Result(Success, running)
      }


    case Event(tr: TestResult, Instruction(i, running)) =>
      self ! TearDown
      goto(CleaningUp) using Result(tr, running)
  }

  when(CleaningUp) {
    case Event(TearDown, result: Result) =>
      for(name <- result.running.components){
        println("Sending tear down to " + name)
        actorRefs(name) ! Teardown
      }
      self ! Free
      goto(CleaningUp) using result

    case Event(Free, Result(result, running)) =>
      for(name <- running.components){
        println("Freeing " + name)
        actorRefs(name) ! Free
      }
      running.monitor ! result
      goto(Idle) using Nil

  }

}


