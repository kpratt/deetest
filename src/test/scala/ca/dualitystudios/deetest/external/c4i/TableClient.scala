package ca.dualitystudios.deetest.external.c4i

import org.sikuli.api._;
import org.sikuli.api.robot.Mouse;
import org.sikuli.api.robot.desktop.DesktopMouse;
import org.sikuli.api.visual.Canvas;
import org.sikuli.api.visual.DesktopCanvas
import java.io.File
import sun.misc.RequestProcessor
;


object TableClient {
  def apply() = new TableClient
}

class TableClient {

  def startC4i() = {

  }

  def mapIsAt(lat: Int, long: Int, zoom: Int): Boolean = (lat, long, zoom) match {
    case(x, y, 4) if showesDownTownCalgary(x, y) =>
      val s = new DesktopScreenRegion()
      val target = new ImageTarget(new File("pictures_icon.png"))
      val r = s.find(target)
      r != null // image found
    case _ => false
  }

  private def showesDownTownCalgary(x: Int, y: Int) = x < 20 && x > 0 & y < 50 & y > 20

}

