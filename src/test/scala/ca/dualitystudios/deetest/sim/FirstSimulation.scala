package ca.dualitystudios.deetest.sim

import org.specs2.mutable.Specification
import java.io.{StringReader, File}
import com.typesafe.config.ConfigFactory
import scala.concurrent.{Await, Future}
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import akka.actor.ActorSystem
import ca.dualitystudios.deetest._
import ca.dualitystudios.deetest.DeeTest._
import org.specs2.execute.{Result, AsResult}
import ca.dualitystudios.deetest.device.{Slave, DeviceClient}


object FirstSimulation extends Specification {

  sequential


  "a" should {
    val slaves: Map[String, Slave] = (for(i <- 1 to 2) yield {
      val reader = new StringReader(
        "deetest.slave.uuid=\"UUID-ABCDFGH-"+i+"\"\n"+
          "akka.remote.netty.port="+(2552+i)+"\n" +
          "deetest.system.name=\""+SYSTEM_NAME+i+"\"\n"
      )
      val config = ConfigFactory.parseReader(reader)
      val ret = (("slave"+i) -> new DeviceClient(true, config).run)
      ret
    }).toMap

    var sys:ActorSystem = null
    try {
      sys = ActorSystem(SYSTEM_NAME, ConfigFactory.
        parseString("akka.remote.netty.port="+(2552)+"\n").
        withFallback(ConfigFactory.load())
      )
    } catch {
      case e : Throwable =>
        println(e.getMessage)
        e.printStackTrace()
        throw new RuntimeException(e)
    }

    "c" in {
      val controller = Controller(new ControllerImpl(slaves))(sys)
      val futureTestResults = controller.run(new Spec {
        def setup: PartialFunction[String, Any] = {
          case "slave1" => ()
          case "slave2" => ()
        }

        def test: TestStructure = List(
          device("slave1"){ _ match { case ctx: Unit =>
            failure("WIN")
          }}
        )

        val actors: List[String] = List("slave1", "slave2")
      })
      val testResults = Await.result(futureTestResults, Duration(30, TimeUnit.MINUTES))
      testResults match {
        case Failure("WIN") => success
        case Success => failure("No Success Allowed")
        case Failure(msg) => failure(msg)
        case _ => failure("We don't know why this failed.")
      }
    }

    "d" in {
      var failsFast = false
      new Spec with AsSpec2Result {

        val controller = Controller(new ControllerImpl(slaves))(sys)


        def setup: PartialFunction[String, Any] = {
          case "slave1" => ()
          case "slave2" => ()
        }

        def test: TestStructure = List(
          device("slave1"){ _ =>
            failsFast = true
            failure("WIN")
          },
          device("slave2"){ _ =>
            failsFast = false
            failure("WIN")
          }
        )

        val actors: List[String] = List("slave1", "slave2")
      }.asResult match {
        case _:org.specs2.execute.Failure => success
        case _ => failure("The failure we wanted was not present")
      }
      failsFast must beTrue

    }
  }

}
