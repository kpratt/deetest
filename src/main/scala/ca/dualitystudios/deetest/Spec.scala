package ca.dualitystudios.deetest

import scala.Predef._


object Spec {

}

trait Spec extends Asserts {
  import TestStructure._
  val actors: List[String] // name -> GUID

  def test: TestStructure

  def setup: PartialFunction[String, Any]
    //case null => throw new SpecException("Actor was given a null identifier.")

  def teardown(name: String, context: Any): Unit = name match {
    case _ =>
  }

  def actor(name: String)(f: (Any)=>TestResult) = (name, f)
  def device = actor(_)

  def asyncronously(branches: List[LeafTree[TestBlock]]): LeafTree[TestBlock] = new AsyncBranch(branches)
  def shuffle(branches: List[LeafTree[TestBlock]]): LeafTree[TestBlock] = new ShuffleBranch(branches)


}


object TestStructure {
  type TestBlock = (String, (Any) => TestResult)

  def apply(block: TestBlock): LeafTree[TestBlock] = new Leaf(block)
  def apply(tree: LeafTree[TestBlock]): TestStructure = new TestStructure(tree)

  implicit def apply(blocks: List[TestBlock]): TestStructure =
    new TestStructure(new Branch(blocks.map(apply(_))))
}


class TestStructure(tree: LeafTree[TestStructure.TestBlock]){
  import TestStructure._
  def testInstance(i: Int): Option[List[TestBlock]] = {
    if(i == 0) {
      Some(
        tree.foldRight(List(_))(_.foldRight(List[TestBlock]())(_ ++ _)) )
    } else None
  }
}


class Leaf[T](data: T) extends LeafTree[T] {
  def foldRight[S](f: T => S)(g: Seq[S] => S): S = f(data)
}

class Branch[T](children: Seq[LeafTree[T]]) extends LeafTree[T] {
  def foldRight[S](f: T => S)(g: Seq[S] => S): S =
    g(children.map(_.foldRight(f)(g)))
}

abstract class LeafTree[T]{
  def map[S](f: T => S): LeafTree[S] = foldRight[LeafTree[S]](x => new Leaf(f(x)))(x => new Branch(x))
  def foldRight[S](f: T => S)(g: Seq[S] => S): S


}

// customize for pattern match

class AsyncBranch(children: Seq[LeafTree[TestStructure.TestBlock]]) extends Branch(children)
class ShuffleBranch(children: Seq[LeafTree[TestStructure.TestBlock]]) extends Branch(children)




/////////////////////////////////////////////////


object ExampleSpec extends Spec {

  import TestStructure._

  override val actors = List(
    "bob",  //UUID.randomUUID() provided by
    "joe"   // the client in device bob's config file
  )

  override def setup = {
    case "bob" => 3
    case "joe" => 5
  }

  override def test = List(
    device("bob"){ case ctx: Int =>
      //have bob do stuff
      ()
    },
    device("joe"){ case ctx: Int =>
      //have joe do stuff
      ()
    }
  )
}