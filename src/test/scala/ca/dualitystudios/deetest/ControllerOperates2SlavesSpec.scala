package ca.dualitystudios.deetest


import org.specs2._
import scala.concurrent.Await
import java.util.concurrent.TimeUnit
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.ActorSystem
import ca.dualitystudios.deetest.device.Slave

object ControllerOperates2SlavesSpec extends mutable.Specification {


  "A controller given 2 slaves" should {
    "run a failing test" in {
      pending
      val spec = new Spec {
        import TestStructure._

        def setup: PartialFunction[String, Any] = {
          case _ => // no setup required
        }

        def test: TestStructure = List(
          actor("one"){ case _ =>
            assert("jargon")    { 1 == 1 } &
            assert("gibberish") { 2 == 2 }
          },
          actor("two"){ case _ =>
            assert("Hello world!") { 1 == 2 }
          }
        )

        val actors: List[String] = List("one", "two")
      }

      implicit val sys = ActorSystem("UnitTests")

      val slave1 = Slave()
      val slave2 = Slave()

      val controller = Controller()

      try {
      val testResult = controller.run(spec)


      Await.result(testResult.map(_ must be equalTo(Failure("Hello world!"))), Duration(30, TimeUnit.SECONDS))
      } catch {
        case e:Throwable =>
          e.printStackTrace()
          failure("shit!")
      }
    }
  }


}
