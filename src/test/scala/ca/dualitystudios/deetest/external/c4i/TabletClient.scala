package ca.dualitystudios.deetest.external.c4i

object TabletClient {
  def apply() = new TabletClient
}

class TabletClient {
  def startC4i() = {

    ()
  }

  def pairWithUser() = {

  }

  //counter clockwise from east
  def orientDevice(degrees: Int) = ()
  def flickUp() = ()
  def openMap(lat:Int, long: Int, zoom: Int) = ()

}
