import sbt._
import Keys._

object DeeTestBuild extends Build {

  val akkaVersion = "2.2.0"

  override lazy val settings = super.settings ++ Seq(
    scalaVersion := "2.10.1",

    resolvers ++= Seq(
      "JavaCV" at "http://maven2.javacv.googlecode.com/git/",
      "JavaCPP" at "http://maven2.javacpp.googlecode.com/git/",
      "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
    ),
    testOptions in Test += Tests.Argument("junitxml"),

    libraryDependencies ++= Seq(
      "com.typesafe.akka"       %%      "akka-actor"      %      akkaVersion,
      "com.typesafe.akka"       %%      "akka-remote"     %      akkaVersion,
      "com.typesafe.akka"       %%      "akka-slf4j"      %      akkaVersion,
      "commons-io"               %      "commons-io"      %      "2.3",
      "org.specs2"              %%      "specs2"          %      "1.14",

      "org.sikuli"               %      "sikuli-api"      %      "1.0.2"      % "test",

      "junit"                    %      "junit"           %      "4.7"        % "test"

    )
  )


  lazy val root = Project(id = "DeeTest",
                            base = file("."),
                            settings = Project.defaultSettings) dependsOn (file("./dsg/node"))
}

