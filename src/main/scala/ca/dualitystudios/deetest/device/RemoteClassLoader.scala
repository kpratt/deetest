package ca.dualitystudios.deetest.device

import akka.pattern.ask
import akka.actor._
import akka.util.Timeout
import java.util.concurrent.TimeUnit
import scala.util.Try
import scala.concurrent.{Future, Await}
import org.apache.commons.io.IOUtils
import scala.concurrent.ExecutionContext.Implicits.global
import scala.Some
import java.io.{ByteArrayInputStream, InputStream}
import java.net.URL
import ca.dualitystudios.deetest.Logger

object RemoteClassLoader {
  def apply(rcf: ActorRef) = new RemoteClassLoader(rcf, Thread.currentThread().getContextClassLoader)
}

class RemoteClassLoader(rcf: ActorRef, parent: ClassLoader) extends ClassLoader(parent) with Logger {
  implicit val timeout = Timeout(5, TimeUnit.MINUTES)

  override def getResourceAsStream(name: String): InputStream = {
    printf("Requesting [ %s ]%n", name)
    val local = parent.getResourceAsStream(name)
    if(local == null){
      val result: Resource = Await.result( rcf.ask(GetResource(name)).mapTo[Resource], timeout.duration )
      result.maybeBytes.map(new ByteArrayInputStream(_)).getOrElse(null)
    } else local
  }

  override def findClass(name: String): Class[_] = {
    (Try {
      super.findClass(name)
    } recover {
      case _ =>
        val result: ClassDefinition =
          Await.result( rcf.ask(GetClass(name)).mapTo[ClassDefinition], timeout.duration )
        result.maybeBytes.map {
          bytes =>
            defineClass(name, bytes, 0, bytes.size)
        } getOrElse {
          throw new ClassNotFoundException("Could not find " + name)
        }
    }).get
  }
}

object RemoteClassFinder {
  def apply(name: String = "RemoteClassLoader")(implicit sys: ActorContext): ActorRef =
    sys.actorOf(Props[RemoteClassFinder](), name)
}

case class GetClass(name: String)
case class ClassDefinition(maybeBytes: Option[Array[Byte]])
case class GetResource(name: String)
case class Resource(maybeBytes: Option[Array[Byte]])

class RemoteClassFinder extends Actor with Logger {

  def receive = {
    case GetResource(name) =>
      val theSender = sender
      Future {
        print("Requested resource [ " + name + " ] ...")
        val stream = getClass.getClassLoader.getResourceAsStream(name)
        if(stream != null) {
          println("Found.")
          Try {
            val array  = IOUtils.toByteArray(stream)
            println("Byte count of resource:" + array.size.toString)
            theSender ! Resource(Some(array))
          }.getOrElse{
            theSender ! Resource(None)
          }
        } else {
          println("Missing.")
          theSender ! Resource(None)
        }
      }
    case GetClass(name) =>
      val theSender = sender
      Future {
        print("Requested class [ " + name + " ] ...")
        val path = name.replace('.', '/') + ".class"
        val stream = getClass.getClassLoader.getResourceAsStream(path)
        if(stream != null) {
          println("Found.")
          Try {
            val array  = IOUtils.toByteArray(stream)
            theSender ! ClassDefinition(Some(array))
          }.getOrElse{
            theSender ! ClassDefinition(None)
          }
        } else {
          println("Missing.")
          theSender ! ClassDefinition(None)
        }
      }
  }
}