package ca.dualitystudios.deetest.device

import akka.actor._
import ca.dualitystudios.deetest._
import scala.concurrent.duration.Duration
import java.util.concurrent.TimeUnit
import java.util.UUID
import scala.util.control.NonFatal
import scala.concurrent.ExecutionContext.Implicits.global
import ca.dualitystudios.deetest.Allocated

object Slave{

  def apply(name: String = "slave")(implicit sys: ActorSystem): ActorRef =
    sys.actorOf(Props[Slave](), name)
}

case class Allocate(name: String, classFinder: ActorRef)
case class AllocationFailed(name: String)
case class Setup(name: String, spec: String)
case class Execute(f: Int)
case class CreateAndExecute(fName: String)
case object Teardown
case object Free
case class TimeOut(testRun: UUID)
case class Print(msg : String)

class Slave extends Actor with Logger {


  def receive = {
    case x =>
      try {
        println("Received message [ " + x.toString + " ]")
        x match {
          case Allocate(name, classFinder) =>
            testRun = UUID.randomUUID()
            val uuid = testRun
            context.system.scheduler.scheduleOnce(Duration(30, TimeUnit.MINUTES)) {
              self ! TimeOut(uuid)
            }
            if(allocate(classFinder))
              sender ! Allocated(name)
            else
              sender ! AllocationFailed(name)

          case TimeOut(uuid) if(uuid == testRun) => abort()
          case Setup(name, specName) =>
            println(specName)
            val f = remoteLoader.loadClass(specName).getConstructor().newInstance()
            println(f.getClass.getName)
            val spec = f.asInstanceOf[Spec]
            setup(name, spec)

          case CreateAndExecute(fName) =>
            println(fName)
            val f = remoteLoader.loadClass(fName).getConstructor().newInstance()
            println(f.getClass.getName)
            val g = f.asInstanceOf[Function1[Any, TestResult]]
            sender ! execute(g)

          case Execute(i) =>
            val tiOp = spec.test.testInstance(0)
            val ti = tiOp.get
            val f = ti(i)._2
            sender ! execute(f)
          case Teardown => teardown()
          case Free => free()
          case Print(msg) => println(msg)
          case y => println(y.toString)
        }
      } catch {
        case NonFatal(e) =>
          System.out.println(e.getMessage)
          e.printStackTrace()
          sender ! Error(e.getMessage, e.getStackTrace.toList.map(x => "%-70s %-20s %-8s".format(x.getFileName, x.getMethodName, x.getLineNumber)) ).toFailure
          abort()
      } finally {
        println(x.toString)
      }
  }

  var allocated = false
  var spec: Spec = null
  var ctx: Any = null
  var remoteLoader: ClassLoader = null
  var testRun: UUID = null
  var name: String = null

  def abort() = {teardown();free()}

  def allocate(remoteFinder: ActorRef): Boolean = {
    if(allocated) false else {
      allocated = true
      val localLoader = this.getClass.getClassLoader
      remoteLoader = new RemoteClassLoader(remoteFinder, localLoader)
      true
    }
  }

  def setup(name: String, spec: Spec): Unit = {
    this.name = name
    this.spec = spec
    ctx = spec.setup(name)
  }

  def execute(f: Any => TestResult): TestResult = {
    print("executing...")
    val r = f(ctx)
    println("done.")
    r
  }

  def teardown() {
    this.spec.teardown(name, ctx)
    this.spec = null
    this.ctx = null
  }

  def free() = {
    testRun = null
    remoteLoader = null
    allocated = false
    System.out.println("Device Freed.")
  }
}
