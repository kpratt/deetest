import sbt._
import Keys._

object DeeTestBuild extends Build {

  override lazy val settings = super.settings ++ Seq(
    scalaVersion := "2.10.1",
    resolvers ++= Seq(
      "JavaCV" at "http://maven2.javacv.googlecode.com/git/",
      "JavaCPP" at "http://maven2.javacpp.googlecode.com/git/",
      "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"
    ),
    testOptions in Test += Tests.Argument("junitxml"),

    libraryDependencies ++= Seq()
  )

  lazy val root = Project(id = "DeeTest_TestProject",
    base = file("."),
    settings = Project.defaultSettings) dependsOn (file("../.."))
}
